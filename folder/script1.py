from dataclasses import dataclass

@dataclass
class month_data:
    name: str
    tem: float
    wetness: int
    velocity_wind: float
    # minor_data: list[int]


if __name__ == '__main__':
    with open('./dataset.txt', 'r') as file:
        lines = file.readlines()
        line = list(map(lambda x: x.split(), lines))
        data_month = []
        for line_info in line:
            data_month.append(month_data(line_info[0], float(line_info[1].split('°С')[0]), int(line_info[2]), float(line_info[4])))
        # print(data_month)
    # f_temp= open('all_temp.txt', 'w')
    # for index in data_month:
    #     f_temp.write(index.tem

    with open('./all_temp.txt', 'w') as file:
        for index in data_month:
            file.write(str(index.tem) + '\n')

    wet_mass = []
    for index in data_month:
        wet_mass.append(index.wetness)
    mean_wet = sum(wet_mass)/len(wet_mass)

    with open('./mean_wet.tmp', 'w') as file:
        file.write(str(mean_wet))



    # openfile
    # collectdata
    # struct_by_season
    # write_data