sum_temp = []
with open('./all_temp.txt', 'r') as file:
    lines = file.readlines()
    line = list(map(lambda x: float(x.strip()), lines))
    mean_val = round(sum(line) / len(line), 4)
    with open('./mean_temp.tmp', 'w') as file:
        file.write(str(mean_val))
