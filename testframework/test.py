import os
from pathlib import Path

if __name__ == '__main__':
    name = []
    value = []
    data_path = Path('./newfolder')
    for filename in os.listdir(data_path):
        if filename.endswith('.tmp'):
            # print(os.path.join(data_path, filename))
            name.append(filename)
            with open(os.path.join(data_path, filename), 'r') as f:
                text = f.read()
                # print(text)
                value.append(float(text))
    # print(name, value, sum(value))
    if sum(value) > 40:
        print('sum =', round(sum(value), 3), '. Bad value.')
    else:
        print('sum =', round(sum(value), 3), '. Okay.')



